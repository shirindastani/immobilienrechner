from immobilienrechner import utils

def test_market_price_real_estate():
    berlin_house = utils.market_price_real_estate(place='berlin', qm=100, type_realestate=utils.RealEstateType.HOUSE)
    berlin_flat = utils.market_price_real_estate(place='berlin', qm=100, type_realestate=utils.RealEstateType.HOUSE)
    assert isinstance(berlin_house, float) and 100.0 < berlin_house < 100_000_000.0
    assert isinstance(berlin_flat, float) and 100.0 < berlin_flat < 100_000_000.0

def test_estate_calculations():
    house = utils.EstateCalculation(price=100_000, notary_percent=0.02, tax_percent=0.03, agent_percent=0.04, equity=10_000, interest_rate=0.02, repay_rate=0.02)

    assert house.notary_cost ==  100_000 * 0.02
    assert house.tax_cost == 100_000 * 0.03
    assert house.agent_cost == 100_000 * 0.04

    assert house.credit == 99_000

    assert house._i == house.interest_rate
    assert house._q == 1 + house._i

    assert house.repay_years() == 35
    assert 0.019 < house.calc_repay_rate(35) < 0.021

    house.interest_rate = 0

    assert house._i > 0
    assert house._q > 1

def test_estate_calculations_months():
    house = utils.EstateCalculation(price=100_000, notary_percent=0.02, tax_percent=0.03, agent_percent=0.04, equity=10_000, interest_rate=0.02, repay_rate=0.02)

    expect = '[Row(zeit=1, vorher=99000.0, zinsen=165.0, tilgung=165.0, ratepromonat=330.0, nachher=98835.0), Row(zeit=2, vorher=98835.0, zinsen=164.72500000000002, tilgung=165.27499999999998, ratepromonat=330.0, nachher=98669.725)]'

    actual = str(list(house.monthly_rows())[:2])

    assert expect == actual

def test_estate_calculations_summary():
    house = utils.EstateCalculation(price=100_000, notary_percent=0.02, tax_percent=0.03, agent_percent=0.04, equity=10_000, interest_rate=0.02, repay_rate=0.02)

    expect = '([Row(zeit=1, vorher=99000.0, zinsen=165.0, tilgung=165.0, ratepromonat=330.0, nachher=98835.0), Row(zeit=2, vorher=98835.0, zinsen=164.72500000000002, tilgung=165.27499999999998, ratepromonat=330.0, nachher=98669.725), Row(zeit=3, vorher=98669.725, zinsen=164.44954166666668, tilgung=165.55045833333332, ratepromonat=330.0, nachher=98504.17454166667), Row(zeit=4, vorher=98504.17454166667, zinsen=164.17362423611112, tilgung=165.82637576388888, ratepromonat=330.0, nachher=98338.34816590277), Row(zeit=5, vorher=98338.34816590277, zinsen=163.8972469431713, tilgung=166.1027530568287, ratepromonat=330.0, nachher=98172.24541284595), Row(zeit=6, vorher=98172.24541284595, zinsen=163.62040902140993, tilgung=166.37959097859007, ratepromonat=330.0, nachher=98005.86582186737), Row(zeit=7, vorher=98005.86582186737, zinsen=163.34310970311228, tilgung=166.65689029688772, ratepromonat=330.0, nachher=97839.20893157048), Row(zeit=8, vorher=97839.20893157048, zinsen=163.06534821928415, tilgung=166.93465178071585, ratepromonat=330.0, nachher=97672.27427978977), Row(zeit=9, vorher=97672.27427978977, zinsen=162.78712379964963, tilgung=167.21287620035037, ratepromonat=330.0, nachher=97505.06140358941), Row(zeit=10, vorher=97505.06140358941, zinsen=162.50843567264903, tilgung=167.49156432735097, ratepromonat=330.0, nachher=97337.56983926205), Row(zeit=11, vorher=97337.56983926205, zinsen=162.22928306543676, tilgung=167.77071693456324, ratepromonat=330.0, nachher=97169.79912232749), Row(zeit=12, vorher=97169.79912232749, zinsen=161.94966520387916, tilgung=168.05033479612084, ratepromonat=330.0, nachher=97001.74878753137)], Row(zeit=1, vorher=99000.0, zinsen=1961.74878753137, tilgung=1998.25121246863, ratepromonat=3960.0, nachher=97001.74878753137))'

    actual = str(house.summary()[1])

    assert expect == actual

def test_additional_functions():
    assert isinstance(i := utils.interest_rate_example(), float) and -1 < i
    assert isinstance(i := utils.agent_price('Hessen'), float) and 0 < i < 20
    assert isinstance(i := utils.tax_on_state('Hessen'), float) and 0 < i < 20
    assert isinstance(i := utils.place_to_state('Frankfurt am Main'), str) and i == 'Hessen'

def test_make_pdf():
    import tempfile
    import io

    expect_bytes = b'%PDF-1.3\n3 0 obj\n<</Type /Page\n/Parent 1 0 R\n/Resources 2 0 R\n/Contents 4 0 R>>\nendobj\n4 0 obj\n<</Filter /FlateDecode /Length 169>>\nstream\nx\x9c\x85\xd0\xbf\x0e\x820\x10\xc7\xf1\x9d\xa7\xf8\x8d:x\xf6\xda^\xff\xac&2\xb8\xdaG\x10\x13\x8d\x89\t!\xe1\xf5-B\x81\t\xd6\xe6>\xdf\xdcU\xe3V)\x12\x8f\xbe\xba$\x9ck\x06[R\n\xe9\x89k\x1a\x9et\x10\x8a\x01AE2\x16\xe9\x81C\xf7\xea>\xcd\x11\xe9=M\x9ck\n\xd6\x0b\xd2\x81\x8c\xc0\x07M\xc6AL \t8\x8d\x03m\x83;21L\x1c\xe1\xbd&\xef\xfeM.\xbd\t{\xb5\x8b\x9d*\xb8-X\x9c\xa3h\xb6\xb5\xb88\xb8\x15\xff\x16\xce\xac\x84\x84\xb7}\x1e\x1aW\\\x02\xfd\x1cp6\x7f^\xdc\t8\x9b\xb7\xd4\xeb\xc0|\xfd\x0f,\x1fTl\nendstream\nendobj\n1 0 obj\n<</Type /Pages\n/Kids [3 0 R ]\n/Count 1\n/MediaBox [0 0 595.28 841.89]\n>>\nendobj\n5 0 obj\n<</Type /Font\n/BaseFont /Times-Bold\n/Subtype /Type1\n/Encoding /WinAnsiEncoding\n>>\nendobj\n6 0 obj\n<</Type /Font\n/BaseFont /Courier\n/Subtype /Type1\n/Encoding /WinAnsiEncoding\n>>\nendobj\n2 0 obj\n<<\n/ProcSet [/PDF /Text /ImageB /ImageC /ImageI]\n/Font <<\n/F1 5 0 R\n/F2 6 0 R\n>>\n/XObject <<\n>>\n>>\nendobj\n7 0 obj\n<<\n/Producer (PyFPDF 1.7.2 http://pyfpdf.googlecode.com/)\n/CreationDate (D:20201230180702)\n>>\nendobj\n8 0 obj\n<<\n/Type /Catalog\n/Pages 1 0 R\n/OpenAction [3 0 R /FitH null]\n/PageLayout /OneColumn\n>>\nendobj\nxref\n0 9\n0000000000 65535 f \n0000000326 00000 n \n0000000604 00000 n \n0000000009 00000 n \n0000000087 00000 n \n0000000413 00000 n \n0000000510 00000 n \n0000000718 00000 n \n0000000827 00000 n \ntrailer\n<<\n/Size 9\n/Root 8 0 R\n/Info 7 0 R\n>>\nstartxref\n930\n%%EOF\n'

    testpdf = utils.make_pdf(header=['1'], rows=['row1'], title='title')

    with tempfile.NamedTemporaryFile(delete=False) as first_out:
        testpdf.output(actual_filename := first_out.name)

    with io.StringIO(expect_bytes.decode(encoding='latin1')) as f:
        with open(actual_filename, encoding='latin-1') as g:
            for expect_line, actual_line in zip(f, g):
                assert (expect_line == actual_line or 'CreationDate' in actual_line)
