from webscraping.database import count_zip, calc_avg_houseprice
from collections import namedtuple
from dataclasses import dataclass
from typing import Iterator
from enum import Enum
from requests_html import HTMLSession
from functools import lru_cache
from itertools import zip_longest
import re
import math
import sys

Row = namedtuple('Row', 'zeit vorher zinsen tilgung ratepromonat nachher')


# recipe from itertools docs
def _grouper(iterable, n, fillvalue=None):
    """Collect data into fixed-length chunks or blocks"""
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


class RealEstateType(Enum):
    APPARTMENT = 'apartment'
    HOUSE = 'house'


def _qmpercity(place: str, type_realestate: RealEstateType, session=HTMLSession()) -> float:
    """Returns qm price for a house or a flat in a german place"""

    zips = count_zip(place)

    if zips >= 1 and type_realestate == RealEstateType.HOUSE:  # ensures that there are enough values in the db for calculation
        price_db = str(calc_avg_houseprice(place))
        price = re.search(r'\d+', price_db)
        return float(price.group())

    else:

        link = f'https://www.homeday.de/de/preisatlas/{place}?map_layer=standard&marketing_type=sell&property_type={type_realestate.value}'

        r = session.get(link)
        price_elem = r.html.find('.price-block__price__average', first=True)

        price_elem = price_elem.text.replace('.', '')  # 4.500 (german 1000 seperator) => 4500
        price = re.search(r'\d+', price_elem)
        return float(price.group())


def market_price_real_estate(place: str, qm: int, type_realestate: RealEstateType) -> float:
    """Return qm price for real estate type('flat', 'apartment') for a place('berlin', 'frankfurt, westend')"""
    r = _qmpercity(place, type_realestate) * qm
    return _qmpercity(place, type_realestate) * qm


@lru_cache
def place_to_state(place: str, key='f384ecc512e24cc59eb4da8b619877cd') -> str:
    """Convert a place string to a concrete state"""
    from opencage.geocoder import OpenCageGeocode
    geocoder = OpenCageGeocode(key)
    location = geocoder.geocode(place, language='de', no_annotations='1')
    state = location[0]['components']['state']
    return state


def tax_on_state(state: str, session=HTMLSession(), memory={}) -> float:
    """Returns tax to be paid in place. Prices are fetched on the internet"""

    if state in memory:
        return memory[state]

    r = session.get('https://www.zinsen-berechnen.de/grunderwerbsteuer/bundeslaender.php')

    prepare_search_string = f'<td>{state}</td><td>'+'{}</td>'
    raw_tax = r.html.search(prepare_search_string)[0].rstrip()

    tax = float(raw_tax.split()[0].replace(',', '.'))

    return tax


def notary_price(session=HTMLSession()) -> float:
    """Return notary percent, that is usually paid"""
    r = session.get('http://www.gnotkg.de/notarkosten-schaetzen.html')

    prepare_search_string = 'mit ca. {}% vom Kaufpreis'
    raw_notary = r.html.search(prepare_search_string)[0].strip()

    notary_price = float(raw_notary.replace(',', '.'))

    return notary_price


def agent_price(state: str, session=HTMLSession()) -> float:
    """Return agent percent, that has to be paid in a state"""
    r = session.get('https://www.homeday.de/de/homeday-makler/maklerprovision/')

    if state in ('Berlin', 'Brandenburg'):
        state = 'Berlin und Brandenburg'

    prepare_search_string = f'<td>{state}</td>\n'+r'<td>{}%'

    if not (m := r.html.search(prepare_search_string)):
        m = r.html.search(prepare_search_string.replace(state, 'Alle anderen Bundesländer i.d.R.'))
        div2 = False
    else:
        div2 = True

    return float(m[0].replace(',', '.')) / (int(div2) + 1)


def interest_rate_example(session=HTMLSession()) -> float:
    """Return interest rate, that has to be paid"""
    r = session.get('https://www.vergleich.de/aktuelle-bauzinsen.html')
    m = r.html.search('15 Jahre">{} %')[0]
    return float(m.replace(',', '.'))

def make_pdf(header, rows, title='', top_font='Times', row_font='Courier'):
    pdf = FPDF()
    pdf.add_page()
    page_width = pdf.w - 2 * pdf.l_margin

    pdf.set_font(top_font,'B',14.0)
    pdf.cell(page_width, 0.0, title, align='C')
    pdf.ln(10)

    pdf.set_font(row_font, '', 12)

    col_width = page_width/len(header)

    pdf.ln(1)
    th = pdf.font_size

    for row in chain([header], rows):
        for row_elem in row:
            pdf.cell(col_width, th, str(row_elem), border=1)
        pdf.ln(th)

    pdf.ln(10)

    return pdf


@dataclass
class EstateCalculation:
    price: float
    notary_percent: float
    tax_percent: float
    agent_percent: float
    equity: float
    interest_rate: float
    repay_rate: float

    @classmethod
    def fromyears(cls, years, **kwargs):
        """Initialize Calculation from repayment years"""
        ec = cls(repay_rate=0.0, **kwargs)
        ec.repay_rate = ec.calc_repay_rate(years)
        return ec

    @property
    def tax_cost(self):
        return self.tax_percent * self.price

    @property
    def agent_cost(self):
        return self.agent_percent * self.price

    @property
    def notary_cost(self):
        return self.notary_percent * self.price

    @property
    def credit(self):
        credit = self.price - self.equity + self.tax_cost \
                 + self.agent_cost \
                 + self.notary_cost
        return credit

    @property
    def _i(self):
        return max(self.interest_rate, sys.float_info.epsilon)  # hack in order to prevent /0

    @property
    def _q(self):
        return 1 + self._i

    def monthly_rows(self) -> Iterator[Row]:
        """Yields monthly payments in row (type: Row) for mortgage loan"""
        assert self.repay_rate > 0

        kreditsumme = self.credit

        zinspromonat = self.interest_rate / 12

        rate = (self.interest_rate + self.repay_rate) * self.credit
        ratepromonat = rate / 12

        vorher = kreditsumme
        monat = 1

        while vorher > 0.00001:

            zinsen = vorher * zinspromonat
            tilgung = ratepromonat - zinsen

            if vorher < ratepromonat:
                ratepromonat = tilgung = vorher

            nachher = vorher - tilgung

            yield Row(monat, vorher, zinsen, tilgung, ratepromonat, nachher)

            vorher = nachher
            monat += 1

    def summary(self):
        """Take rows calc by loancalc and give a summary, {year: (rows, summary), ....}"""

        years_in_chunks = _grouper(self.monthly_rows(), 12)

        out = {}

        for year, rows in enumerate(years_in_chunks, start=1):
            rows = [r for r in rows if r]
            out[year] = (rows,
                         Row(year,
                             vorher=rows[0].vorher,
                             zinsen=sum(r.zinsen for r in rows),
                             tilgung=sum(r.tilgung for r in rows),
                             ratepromonat=sum(r.ratepromonat for r in rows),
                             nachher=rows[-1].nachher))

        return out

    def repay_years(self) -> int:
        """Calculate repayment years for mortgage loan, but repay_rate > 0"""
        assert self.repay_rate > 0

        i, q = self._i, self._q
        R = (self.repay_rate + i) * self.credit
        S_0 = self.credit

        numerator = -math.log(1 - i * S_0 / R)
        denominator = math.log(q)

        result = numerator / denominator
        result = math.floor(result)

        return result

    def calc_repay_rate(self, years) -> float:
        """Calculate repayment rate for mortgage loan"""
        i, q = self._i, self._q
        n = years
        S_0 = self.credit

        R = S_0 * ((q ** n * i) / (q ** n - 1))

        R_percent = R / self.credit

        repay_rate = R_percent - i

        return repay_rate

