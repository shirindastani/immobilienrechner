from PyQt5.QtWidgets import QMainWindow, QTreeWidgetItem, QFileDialog
from PyQt5.QtCore import QFile
from PyQt5.uic import loadUi
from threading import Thread
# Loads resource file, so not a useless import
# Makes loadui possible
from . import resources
from . import utils

import csv
import logging


class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()

        fileh = QFile(':/icons/rechner.ui')
        fileh.open(QFile.ReadOnly)
        loadUi(fileh, self)
        fileh.close()

        self.pbLoadExample.clicked.connect(lambda: Thread(target=self.load_example).start())

        self.pbSpeichern.clicked.connect(self.save_table)

        self._enable_disable_repay_spinboxes(True)

        self.rbCalcYears.clicked.connect(lambda: self._enable_disable_repay_spinboxes(True))
        self.rbCalcRate.clicked.connect(lambda: self._enable_disable_repay_spinboxes(False))

        self.pbCalc.clicked.connect(self.handle_calc)

        self.tabWidget.setCurrentIndex(0)
        self.sbPrice.setFocus()

        self.show()

    def save_table(self):
        try:
            if not (filename := self.saveFileDialog()):
                return
            pdf = utils.make_pdf(header='Monat Vorher Zinsen Tilgung Rate Nachher'.split(),
                                 rows=(map(lambda e: f'{e:.2f}', row) for row in self.ec.monthly_rows()),
                                 title='Kreditberechnung')
            pdf.output(filename)
        except:
            logging.error('Could not save table to file')


    def saveFileDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()","","Pdf File (*.pdf)", options=options)
        return fileName

    def handle_calc(self):
        'Calculate repayment years or repayment rate'
        arguments = { 'price':self.sbPrice.value(),
                      'notary_percent':self.dsNotary.value() / 100.0,
                      'tax_percent':self.dsTax.value() / 100.0,
                      'agent_percent':self.dsAgent.value() / 100.0,
                      'equity':self.sbEquity.value(),
                      'interest_rate':self.dsInterestRate.value() / 100.0}

        if self.is_repayyears_calced:
            repay_rate = self.dsRepayRate.value() / 100.0
            self.ec = utils.EstateCalculation(**arguments, repay_rate=repay_rate)
            self.sbRepayYears.setValue(self.ec.repay_years())
        else:
            years = self.sbRepayYears.value()
            self.ec = utils.EstateCalculation.fromyears(years=years, **arguments)
            self.dsRepayRate.setValue(self.ec.repay_rate * 100)

        self.dsCredit.setValue(self.ec.credit)

        self.currentSummary = self.ec.summary()

        self.insertintotree(self.currentSummary)

        self.insertintotree(self.currentSummary)

    def insertintotree(self, summary):
        """Insert summary into the tree widget"""
        self.treeWidget.clear()
        for _, (months, sumyear) in summary.items():
            yearitem = QTreeWidgetItem([f'{s:.2f}' if i != 0 else f'{s:.0f}' for i, s in enumerate(sumyear)])

            for month in months:
                row = [f'{s:.2f}' for s in month]
                row[0] = f'{float(row[0]):.0f}'
                row[0] = f'Monat ({row[0]})'
                yearitem.addChild(QTreeWidgetItem(row))

            self.treeWidget.addTopLevelItem(yearitem)

    def _enable_disable_repay_spinboxes(self, is_repayyears_calced):
        """Enable or disable the correct reapay-* spinbox, according to what user wants to calculate"""
        self.is_repayyears_calced = is_repayyears_calced
        self.dsRepayRate.setEnabled(self.is_repayyears_calced)
        self.sbRepayYears.setEnabled(not self.is_repayyears_calced)

        if self.is_repayyears_calced:
            self.dsRepayRate.setFocus()
        else:
            self.sbRepayYears.setFocus()

    def load_example(self):
        """Fetch price information about place and set price lineedit"""
        Thread(target=self._load_tax_agent).start()
        Thread(target=self._load_notary).start()
        Thread(target=self._load_interest_rate).start()

        place = self.lePlace.text()
        qm = self.sbQm.value()
        type_estate = utils.RealEstateType.HOUSE if self.rbHouse.isChecked() else utils.RealEstateType.APPARTMENT

        try:
            price = int(utils.market_price_real_estate(place, qm, type_estate))
            self.sbPrice.setValue(price)
        except:
            logging.error('Could not load price information')

        self.dsRepayRate.setValue(2.0)

    def _load_tax_agent(self):
        """Fetch notaty price and set the spin box accordingly"""
        try:
            state = utils.place_to_state(self.lePlace.text())
        except:
            logging.error('Could not determine state of place')
            return
        try:
            self.dsTax.setValue(utils.tax_on_state(state))
        except:
            logging.error('Could not load tax cost')

        try:
            self.dsAgent.setValue(utils.agent_price(state))
        except:
            logging.error('Could not load tax cost')
            return

    def _load_notary(self):
        try:
            self.dsNotary.setValue(utils.notary_price())
        except:
            logging.error('Could not load notary information')

    def _load_interest_rate(self):
        try:
            self.dsInterestRate.setValue(utils.interest_rate_example())
        except:
            logging.error('Could not load interest rate information')
