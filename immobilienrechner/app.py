from PyQt5.QtWidgets import QApplication
from immobilienrechner.window import MainWindow

def main():
    app = QApplication([])
    win = MainWindow()
    app.exec_()


if __name__ == '__main__':
    main()
