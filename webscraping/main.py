# -*- coding: utf-8 -*-
# Local Imports
from webscraping.tools import settings
from webscraping.database import duplicate_check, insert_results, get_random_zip

# Standard Library Imports
import re
import pickle
import logging
from time import sleep

# Additional Library Imports
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as ec

# Advanced settings for browser
driver = settings.get_driver(headless=False)
logger = logging.getLogger(__name__)
timeout = 20


# Cookie handling
def check_cookie_box_visibility():
    """
    Check function with load wait to check if cookie box is visible and if so then execute script
    Todo: Find a workaround for hard coded breaks in DOM objecten in connection with Javascript
    """
    cookie_box_is_visible = bool(False)

    try:
        sleep(2)  # Hardcoded sleep function necessary because of shadow-root DOM element on website
        cookie_box = WebDriverWait(driver, timeout).until(lambda d: d.execute_script(
            'return document.getElementById("usercentrics-root").shadowRoot.'
            'querySelector("div[data-testid]").querySelector("div[data-testid]")'))
        cookie_box_is_visible = True

    except NoSuchElementException:
        print("Element has not been found")

    except TimeoutException:
        print("Timed out waiting for page to load cookie box")

    if cookie_box_is_visible:
        accept_cookies()
        return True


def accept_cookies():
    """
    Accepts cookies if they have been found by function check_cookie_box_visibility
    """
    javascript = 'return document.getElementById("usercentrics-root").shadowRoot.' \
                 'querySelector("div[data-testid]").querySelector("div[data-testid]").' \
                 'querySelector("footer").querySelector("div[data-testid]").querySelector("div").' \
                 'querySelector("[data-testid=uc-accept-all-button]")'

    cookie_button = driver.execute_script(javascript)
    cookie_button.click()


def get_load_cookies():
    """
    Function is getting and loading/using cookies from site in order to prevent cookie box appearing every time
    """
    pickle.dump(driver.get_cookies(), open("cookies.pkl", "wb"))
    for cookie in pickle.load(open("cookies.pkl", "rb")):
        driver.add_cookie(cookie)


# Webscraping
def webscraping_start_page(zip_code: str):
    """
    Function is searching for offers of a specific PLZ on the landing page of https://www.immonet.de/
    Parameters: zip_code (str): Explain
    """
    driver.get("https://www.immonet.de/")
    search_bar = driver.find_element_by_xpath('//*[@id="location"]').send_keys(zip_code + Keys.ENTER), sleep(1)
    buy_button = driver.find_element_by_xpath('//*[@id="btn-insearch-marketingtype-buy"]').click(), sleep(1)
    dropdown_box = driver.find_element_by_xpath('//*[@id="estate-type"]').click(), sleep(1)
    house_select = driver.find_element_by_xpath('//*[@id="estate-type"]/option[2]').click(), sleep(1)
    find_button = driver.find_element_by_xpath('//*[@id="btn-int-find-immoobjects"]').click(), sleep(1)


def webscraping_results_pages():
    """
    Function is getting all search results of all pages found by webscraping_start_page()
    and is inserting them to the database in case the offer has not been added to the database before
    (identified by the unique ref-identifier of immonet)
    """
    while check_next_page()[0]:

        results = driver.find_elements_by_xpath("//*[@class='col-xs-12 place-over-understitial sel-bg-gray-lighter']"
                                                "/div/div/div//*[@class='flex-grow-1 display-flex-sm']"
                                                "//*[@class='flex-grow-1 display-flex flex-direction-column "
                                                "box-25 overflow-hidden cursor-hand']/div/div/a")

        results_list = []

        for result in results:
            result_item = parse_result(result)
            # Check if all required elements (zip, price and area) could have been fetched
            if result_item[0] == "" or result_item[1] == "" or result_item[2] == "":
                logger.exception("Element will not be inserted to DB as required values are missing.")
            else:
                # Perform duplicate check based on ref
                ref = result_item[3]
                if duplicate_check(ref):
                    results_list.append(result_item)
                    print("Added", result_item, "to list and will be inserted to database in a batch")
                else:
                    print("Ref", ref, "is already in the database and will thus not be added.")

        insert_results(results_list)

        url_next_page = check_next_page()[1]
        driver.get(url_next_page)
        sleep(3)


def parse_result(result):
    """
    Function is parsing the details / information of a single result page
    Parameters: result
    Returns: tuple: result_item_tuple containing information of the zip, price, area and ref of a result page
    """
    item_dict = dict(zip='', price='', first_area='', ref='')
    url = result.get_attribute("href")
    javascript = "window.open('" + url + "'"",'_blank')"
    driver.execute_script(javascript)
    driver.switch_to.window(driver.window_handles[1])

    try:
        address_xpath = '//*[@id="top"]/div[1]/div[2]/div[4]/main/div[3]/div[2]/div/div[3]/span/p'
        address_is_present = ec.presence_of_element_located((By.XPATH, address_xpath))
        WebDriverWait(driver, timeout).until(address_is_present)
        address_string = str(driver.find_element_by_xpath(address_xpath).text.replace("\n", " "))

        plz = re.match('^.*(?P<zipcode>\d{5}).*$', address_string).groupdict()['zipcode']
        price = str(driver.find_element_by_xpath('//*[@id="kfpriceValue"]').text.replace(' €', ''))
        price_without_decimal = price.replace('.', '')
        first_area = str(driver.find_element_by_xpath('//*[@id="kffirstareaValue"]').text.replace(' m²', ''))
        ref = str(re.sub(r'https://www.immonet.de/angebot/', '', url))

        item_dict.update({"zip": plz, "price": price_without_decimal, "first_area": first_area, "ref": ref})

    except NoSuchElementException:
        logger.exception("Element from url", url, "has not been found as object has no declared price.")

        pass

    sleep(1)
    driver.close()
    driver.switch_to.window(driver.window_handles[0])

    result_item_tuple = tuple(item_dict.values())
    return result_item_tuple


def check_next_page():
    """
    Function is checking if there are further pages of search results
    Returns: False (bool) in case there are no further pages of search results
             True (bool), in case there are further pages, url of the next page to scrape (str) and current page (int)
    """
    html_source = driver.page_source
    if 'col-sm-3 col-xs-1 pull-right text-right grey' in html_source:
        return False
    else:
        url = driver.current_url
        if "&page=" in url:
            url_1, page = url.split('&page=')
            next_page_int = int(page) + 1
            next_page_str = '&page=' + str(next_page_int)
            url_next_page = url_1 + next_page_str
        else:
            page = 1
            url_next_page = url + '&page=2'

        return True, url_next_page, page


def webscraping_execute():
    """
    Function is executing web scraping
    """
    driver.get("https://www.immonet.de/")
    check_cookie_box_visibility()
    sleep(3)
    get_load_cookies()
    sleep(2)
    zip_code = get_random_zip()
    webscraping_start_page(zip_code)
    sleep(2)
    webscraping_results_pages()