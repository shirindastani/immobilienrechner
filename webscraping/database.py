import mysql.connector
import os

# TODO: Optimize connection usage, reuse connections, pooling
#       Set up separate user configuration for QT with limited access (no write permissions and limited view)


def create_db_connection():
    base_dir = os.path.dirname(os.path.realpath(__file__))
    config_file = os.path.join(
        base_dir,
        'my.ini'
    )
    try:
        connection = mysql.connector.connect(option_files=config_file)
        print("MySQL Database connection successful")

    except ConnectionError as err:
        print(f"Error: '{err}'")

    return connection


def read_query(query):
    connection = create_db_connection()
    cursor = connection.cursor()

    try:
        cursor.execute(query)
        result = cursor.fetchall()
        return result
    except ConnectionError as err:
        print(f"Error: '{err}'")

    cursor.close()
    connection.close()


def executemany_query(sql_statement, values_list):
    connection = create_db_connection()
    cursor = connection.cursor()

    try:
        cursor.executemany(sql_statement, values_list)
        connection.commit()
        print("Query successful")

    except ConnectionError as err:
        print(f"Error: '{err}'")

    cursor.close()
    connection.close()


def duplicate_check(ref):
    """
    Function is checking if offer has already been added to db
    Parameters: ref (str): identified by unique ref of immonet
    Returns: True (bool) if no record has been found
             False (bool) if one or more records have been found
    """
    duplicate_check_query = "SELECT * FROM houseprices WHERE ref =" + ref + ""
    result = read_query(duplicate_check_query)

    if len(result) == 0:
        return True
    else:
        return False


def insert_results(results_list):
    """
    Function is inserting all items to db in a batch using executemany_query function
    Parameters: results_list (list): List containing all offer items of a result page
    """
    sql_insert_statement = "INSERT INTO houseprices (zip, price, first_area, ref) VALUES (%s, %s, %s, %s)"
    executemany_query(sql_insert_statement, results_list)


def get_random_zip():
    """
    Function provides a random zip from db, table plz_ort
    Returns: zip (str)
    """
    random_zip_query = "SELECT * FROM plz_ort ORDER BY RAND() LIMIT 1"
    result = read_query(random_zip_query)
    zip = result[0][2]

    return zip


def count_zip(zip_code: str):
    """
    Function is counting rows containing given zip code
    Parameters: zip_code (str)
    Returns: amount of rows containing zip (int)
    """
    count_zip_query = "SELECT COUNT(*) FROM houseprices WHERE zip =" + zip_code + ""
    result = read_query(count_zip_query)

    return int(result[0][0])


def calc_avg_houseprice(zip_code: str):
    """
    Function is calculating the average houseprice
    Parameters: zip_code (str)
    Returns: average house price per square meter (int)
    """
    calc_avg_price_query = "SELECT AVG(price/first_area)  FROM houseprices WHERE zip =" + zip_code + ""
    result = read_query(calc_avg_price_query)

    return int(result[0][0])